include <pie.scad>;

alturaPieza = 18;

rParedExterior = 200;
grosorParedExterior = 8;

rParedInterior = rParedExterior-50;
grosorParedInterior = 5;

grosorSuelo=3;
alturaIman = 2.1;
rIman=12/2;
nImanes=12;


anchoAnclaje=10;
grosorAnclaje=6;
altoAnclaje=10;
agujeroAnclaje=6;
anchoAgujeroAnclaje=4;

rPerf=12/2;
rPerf2=rPerf;
nAgujeros=60;

;

anchoAncla=4;
altoAncla=6;
grAncla=4;
margen=0;

grosorTapa=4;

grNucleo=10;
alturaNucleo=20;

angulo=360/nAgujeros*5;
calidad = 200;

$fn=calidad;


module huecoIman(){
	 translate([rParedExterior-(rParedExterior-rParedInterior)/2,0,(-alturaPieza/2)+alturaIman]) cylinder(r=rIman,h=alturaIman,center=true);
}

module huecoImanes(){
  for (i=[0:nImanes-1]){
    rotate([0,0,(angulo/2)+i*(360/nImanes)]) huecoIman();
  }
  
}

module corona(re,ri,alt) {
	difference() {
		cylinder(r=re,h=alt,center=true);
		cylinder(r=ri,h=alt+1,center=true);
	}
}


module perforacion() {
	translate([0,0,grosorSuelo/2]) rotate([0,90,0]) cylinder(r=rPerf,h=2*grosorParedExterior,center=true);
}

module perforacionInterior() {
	rotate([0,90,0]) cylinder(r=rPerf,h=2*(grosorParedInterior+grNucleo),center=true);
}


module paredExterior() {
	desp=360/nAgujeros/2;
	difference() {
		corona(rParedExterior+grosorParedExterior,rParedExterior,alturaPieza);
		
		for (i=[0:nAgujeros-1]){
			rotate ([0,0,i*360/nAgujeros+desp])
				translate([rParedExterior,0,0]) perforacion();
		}
	}
}

module paredInterior() {
	desp=360/nAgujeros/2;
	difference() {
		corona(rParedInterior+grosorParedInterior,rParedInterior,alturaPieza);
			
//		for (i=[0:nAgujeros-1]){
//			rotate ([0,0,i*360/nAgujeros*2+desp])
//				translate([rParedInterior+grosorParedInterior,0,((alturaPieza-altoAncla)/2)-margen]) hoyo();
//		}
	}
}

module suelo() {
	difference() {
	translate([0,0,-(alturaPieza-grosorSuelo)/2]) corona(rParedExterior+grosorParedExterior,rParedInterior,grosorSuelo);
	huecoImanes();
	}
}

module gancho(){
	color("blue") cube([anchoAncla,anchoAncla,altoAncla],center=true);
}
//gancho();


module anclaje(){
	difference(){
		cube([anchoAnclaje,grosorAnclaje,altoAnclaje],center=true);
		cube([agujeroAnclaje,grosorAnclaje+1,anchoAgujeroAnclaje],center=true);
	}
}


module anclajes(){
	anclaje();
	translate([anchoAnclaje-1,0,0]) anclaje();
//	translate([anchoAnclaje-1,0,altoAnclaje]) anclaje();
//	translate([0,0,altoAnclaje]) anclaje();
}

module piezaEntera() {
	paredExterior();
	paredInterior();
	suelo();
}

module piezaSeccionada(){
	intersection(){
		union(){
			piezaEntera();
		
			translate([rParedExterior-grosorParedExterior-5,0,-((alturaPieza/2)-(grosorSuelo)-altoAnclaje/2)]) anclajes();
		
			rotate([0,0,angulo]) translate([rParedExterior-grosorParedExterior-5,0,-((alturaPieza/2)-(grosorSuelo)-altoAnclaje/2)]) anclajes();
		}
		translate([0,0,-(alturaPieza+1)/2]) pie(rParedExterior+grosorParedExterior+1,angulo,alturaPieza+1,0);
	}
}




module tapa(){
	desp=360/nAgujeros/2;
	intersection() {
		pie(rParedExterior+grosorParedExterior+1,angulo,100,0);
		union(){
			corona(rParedExterior+grosorParedExterior,rParedInterior,grosorTapa);
			for (i=[0:nAgujeros-1]){
				rotate ([0,0,i*360/nAgujeros*2+desp])
					translate([rParedExterior-(anchoAncla)/2,0,(grosorTapa+altoAncla)/2]) gancho();
			}
			for (i=[0:nAgujeros-1]){
				rotate ([0,0,i*360/nAgujeros*2+desp])
					color("red")translate([rParedInterior+grosorParedInterior+anchoAncla/2,0,(grosorTapa+altoAncla)/2]) gancho();
		}
		}
	}
}

module piezaConSoporte(){

	difference() {
		piezaSeccionada();
		ganchos();
		
	}
}

module ganchos() {
	desp=360/nAgujeros;
	rotate([0,0,desp])
		translate([rParedInterior,0,0])
		perforacionInterior();	
	rotate([0,0,angulo-desp])
		translate([rParedInterior,0,0])
			perforacionInterior();
}

//translate([0,0,30]) tapa();

piezaSeccionada();
//piezaEntera();
//piezaConSoporte();

module aroNucleo() {
	desp=360/nAgujeros;
	difference(){
		cylinder(r=rParedInterior,h=alturaNucleo,center=true);
		cylinder(r=rParedInterior-grNucleo,h=alturaPieza+2,center=true);
		for (i=[0:12-1]){
			rotate([0,0,i*360/12])
				ganchos();
		}
	}

}

module aroNucleoSeccion() {
	intersection(){
		translate([0,0,-alturaPieza/2]) pie(rParedInterior,angulo*2,alturaPieza+3,angulo/2);
		color("gray") aroNucleo();
	}
}

//aroNucleoSeccion();
//aroNucleo();




module corazon() {
	difference() {
		cylinder(r=rParedInterior-grNucleo,h=alturaPieza,center=true);
		translate([0,0,5])
			cylinder(r=rParedInterior-grNucleo-4,h=alturaPieza+1,center=true);
		for (i=[0:12-1]){
			rotate([0,0,i*360/12])
				ganchos();
		}
	}
	rotate([0,0,angulo*2]) translate([rParedInterior-grNucleo-18,0,-((alturaPieza/2)-(grosorSuelo)-altoAnclaje/2)]) anclajes();
	translate([rParedInterior-grNucleo-18,0,-((alturaPieza/2)-(grosorSuelo)-altoAnclaje/2)]) anclajes();

}



module corazonPartido() {
	intersection() {
		corazon();
	translate([0,0,-alturaPieza*2/2])
		pie(rParedInterior-grNucleo+10,angulo*2,alturaPieza*2,0);
	}
}

//corazon();
//color("orange")corazonPartido();

//corona(12/2,10/2,grosorParedInterior+grNucleo+4);
