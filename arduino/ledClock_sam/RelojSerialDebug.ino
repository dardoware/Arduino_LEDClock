void relojSerial(DateTime ahorita) { 
  Serial.print(ahorita.hour(), DEC);
  Serial.print(':');
  Serial.print(ahorita.minute(), DEC);
  Serial.print(':');
  Serial.print(ahorita.second(), DEC);
  Serial.print(' ');
  Serial.print(ahorita.day(), DEC);
  Serial.print('/');
  Serial.print(ahorita.month(), DEC);
  Serial.print('/');
  Serial.print(ahorita.year(), DEC);
}

void ledSerial(byte h, byte m, byte s) {
    Serial.print("Led hora: ");
    Serial.println(h);
    Serial.print("Led minutos: ");
    Serial.println(m);
    Serial.print("Led segundos: ");
    Serial.println(s);  
  }
