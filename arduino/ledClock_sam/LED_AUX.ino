void fijarPaleta(byte p) {
   switch (p) {
    case 0: COLOR_PUNTOS=pFuego[0];  // Fuego
            COLOR_SEGUNDOS=pFuego[1];
            COLOR_MINUTOS=pFuego[2];
            COLOR_HORAS=pFuego[3];
            COLOR_APAGADO=pFuego[4];           
            break;
    case 1: COLOR_PUNTOS=pNieve[0];  // Nieve
            COLOR_SEGUNDOS=pNieve[1];
            COLOR_MINUTOS=pNieve[2];
            COLOR_HORAS=pNieve[3];
            COLOR_APAGADO=pNieve[4];
            break;
    case 2: COLOR_PUNTOS=pGominola[0];  // Gominola
            COLOR_SEGUNDOS=pGominola[1];
            COLOR_MINUTOS=pGominola[2];
            COLOR_HORAS=pGominola[3];
            COLOR_APAGADO=pGominola[4];
            break;
    case 3: COLOR_PUNTOS=pPlaneta[0];  // Planeta
            COLOR_SEGUNDOS=pPlaneta[1];
            COLOR_MINUTOS=pPlaneta[2];
            COLOR_HORAS=pPlaneta[3];
            COLOR_APAGADO=pPlaneta[4];
            break;
    case 4: COLOR_PUNTOS=pGrannySmith[0];  // GrannySmith
            COLOR_SEGUNDOS=pGrannySmith[1];
            COLOR_MINUTOS=pGrannySmith[2];
            COLOR_HORAS=pGrannySmith[3];
            COLOR_APAGADO=pGrannySmith[4];
            break;
    case 5: COLOR_PUNTOS=pCheeseCake[0];  // CheeseCake
            COLOR_SEGUNDOS=pCheeseCake[1];
            COLOR_MINUTOS=pCheeseCake[2];
            COLOR_HORAS=pCheeseCake[3];
            COLOR_APAGADO=pCheeseCake[4];
            break;
     default:
            p=2;
            break;      
  }
}

void apagarLeds(CRGB color) {
   for(int i = 0 ; i < NUM_LEDS-1 ; i++) {
    corona[i]=color;
  }
}

void puntosHora(CRGB color) {
  for(int i = 0 ; i < NUM_LEDS-1 ; i = i + 5) {
    corona[i]= color;   
  }
  corona[0]=color;
}

void puntosCuartos(CRGB color) {
  for(int i = 0 ; i < NUM_LEDS-1 ; i = i + 15) {
    corona[i]=color;
  }
  corona[0]=color;
}

int venticuatroDoce(int n) {
  int resultado;
  resultado = n % 12;
  return resultado;
}

byte es60(int n) {
  int r;
  if (n>=0) {
    r= n%60;
  }
  else {
    r = n+60;
  }
  return r;
}
