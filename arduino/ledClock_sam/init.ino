void inicializacion(){
  if (DEBUG > 0) {
    Serial.begin(SERIAL_SPEED);
    Serial.println(DESC);
    Serial.println();
    Serial.println("Comenzando inicializacion");
    Serial.println();
  } 
  
  if (DEBUG > 0) {
    Serial.println("Esperando FastLed...");
  }
  
  FastLED.addLeds<CHIPSET, LED_DATA_PIN, LED_CLOCK_PIN, ORDEN_COLOR>(corona, NUM_LEDS);
  
    if (DEBUG > 0) {
    Serial.println("FastLed OK");
    Serial.println("");
  }
  
  if (DEBUG > 0) {
    Serial.println("Esperando i2C...");
  }
  Wire.begin();
  
  if (DEBUG > 0) {
    Serial.println("i2c: OK ");
    Serial.println();
  }
  
  if (DEBUG > 0) {
    Serial.println("Esperando RTC...");
  }
  rtc.begin();
  
  if (DEBUG > 0) {
    Serial.println("RTC: Iniciado OK ");
    Serial.println();
  }
  
  if (DEBUG>0) {
    Serial.println("Aplicando configuracion de botones");
  }
  
  boton1.debounceTime   = DEBOUNCE;   
  boton1.multiclickTime = T_MULTICLICK;  
  boton1.longClickTime  = T_LONGCLICK; 
  
  if (DEBUG>0) {
    Serial.println("Aplicada configuracion de botones ");
  }
  
  if (! rtc.isrunning()) {
    if (DEBUG > 0) {
      Serial.println("Intentamos porner en hora el RTC");
      Serial.println();
    }
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    
    if (DEBUG > 0) {
      Serial.println("RTC no esta corriendo pero no te preocupes.");
      Serial.println("Se puede mostrar una demo con leds... (se esta activa)");
      Serial.println();
    }
    if (DEMO == 1) {
      demoLeds();
    }
  }
  else {
    if (DEBUG>0) {
      DateTime muestra = rtc.now();
      relojSerial(muestra);
      Serial.println("Esta en hora");
      Serial.println();
    }
  }
  
  if (DEBUG>0) {
    Serial.println("Inicializacion finalizada");
    Serial.println();
  }
}
  
