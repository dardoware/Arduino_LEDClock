void modoArcos(DateTime laHora, byte p, byte b) {
  fijarPaleta(p);
  apagarLeds(COLOR_APAGADO);
  
  //FastLED.showColor(COLOR_APAGADO);
  //puntosHora(COLOR_PUNTOS);
  //puntosCuartos(COLOR_PUNTOS);
 
  
  int mm_led = laHora.minute();  
  int ss_led = laHora.second();   
  //int hh_led=map(laHora.hour(),0,11,0,59);
  int hh_led=venticuatroDoce(laHora.hour())*5;
  
  //Calculo de led anterior y posterior para las horas (manecilla gruesa)
  
  for (int i=0;i<hh_led;i++){
    corona[i]=COLOR_HORAS;
  }
  
   for (int i=0;i<mm_led;i++){
    corona[i]+=COLOR_MINUTOS;
  }
  
  
  if (DEBUG > 0) {
    ledSerial(hh_led, mm_led, ss_led); 
  }
}
