void modoEstelas(DateTime laHora, byte p, byte b) {
  
  fijarPaleta(p);  
  apagarLeds(COLOR_APAGADO);
  
  //FastLED.showColor(COLOR_APAGADO);
  puntosHora(COLOR_PUNTOS);
 
  
  int mm_led = laHora.minute();  
  int ss_led = laHora.second();   
  int hh_led=venticuatroDoce(laHora.hour())*5;
  
   
  corona[es60(hh_led-1)]= COLOR_HORAS;
  corona[hh_led]= COLOR_HORAS;
  corona[es60(hh_led+1)]= COLOR_HORAS;
  
  corona[mm_led] = COLOR_MINUTOS;
  
 
  corona[ss_led]=COLOR_SEGUNDOS;
  
  for (int i=0; i<10;i++) {
    corona[es60(ss_led-i)]=COLOR_SEGUNDOS; 
    corona[es60(ss_led-i)].fadeLightBy((255/9)*i);
    Serial.print("led ");
    Serial.print(es60(ss_led-i));
    Serial.print(" - Color: (");
    Serial.print(corona[es60(ss_led-i)].r);
    Serial.print(",");
    Serial.print(corona[es60(ss_led-i)].g);
    Serial.print(",");
    Serial.print(corona[es60(ss_led-i)].b);
    Serial.println(")");
  }
  
 

  
  
  if (DEBUG > 0) {
    ledSerial(hh_led, mm_led, ss_led); 
  }
}
