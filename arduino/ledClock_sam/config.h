/* Parmetros fijos. Estos no suelen cambiar
*  o solo cambian entre versiones mayores
*  Cambialos para ajustarlo a tus necesidades
*/
#define VERSION "Betelgeuse"                  // Version 1.1: Betelgeuse, contelacion de Orion
#define DESC "Reloj Led controlado por Arduino"      
#define AUTOR "Samuel Villafranca"        
#define SERIAL_SPEED 115200

#define DEBUG 1                  // 0: No ; 1: Si
#define DEMO  1                  // 0: No ; 1: Si

// Parametros de los LED:
#define CHIPSET       WS2801        // WS2801, WS2811
#define ORDEN_COLOR   RGB           //RGB, RBG. GRB, GBR, etc..
#define NUM_LEDS      60
#define BRILLO_MAX    255
#define BRILLO_MIN    20

// Parametros de los botones
#define DEBOUNCE       20
#define T_MULTICLICK   250
#define T_LONGCLICK    1000

//Pines Arduino
#define BOTON1_PIN      4
#define LED_DATA_PIN    11
#define LED_CLOCK_PIN   13

#define SENSOR_LUZ_PIN  A0
#define SENSOR_TEMP_PIN A1

// Otras definiciones auxiliares necesarias
#define TOTAL_MODOS 4
