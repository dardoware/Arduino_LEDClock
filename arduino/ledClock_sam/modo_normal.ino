void modoNormal(DateTime laHora, byte p, byte b) {
  fijarPaleta(p);
  apagarLeds(COLOR_APAGADO);
  
  //FastLED.showColor(COLOR_APAGADO);
  puntosHora(COLOR_PUNTOS);
  //puntosCuartos(COLOR_PUNTOS);
  
  
  // Posicion en la esfera de cada menecilla
  
  int mm_led = laHora.minute();  
  int ss_led = laHora.second();   
  int hh_led=venticuatroDoce(laHora.hour())*5;
  
  //Calculo de led anterior y posterior para las horas (manecilla gruesa)
  
  
  corona[es60(hh_led-1)]= COLOR_HORAS;
  corona[hh_led]= COLOR_HORAS;
  corona[es60(hh_led+1)]= COLOR_HORAS;
  
  corona[mm_led]= COLOR_MINUTOS;
  corona[ss_led]= COLOR_SEGUNDOS;
  if (DEBUG > 0) {
    ledSerial(hh_led, mm_led, ss_led);  
  }
}


