#include <ClickButton.h>

/*********************************************************
*
*  lClock: Reloj LED con Arduino
*
*  Proyecto por Samuel Villafranca Gómez
* 
*  Mas informacion: http://wiki.makespacemadrid.org
*
*  Cualquier persona es libre de utilizarlo para cualquier proposito,
*  siempre y cuando cite la fuente original.
*  Al Cesar lo que es del Cesar.
*  
**********************************************************/
 

//Libreras

#include <FastLED.h>
#include <Wire.h>
#include "paletas.h"
#include "RTClib.h"
#include "config.h"
#include "ClickButton.h"

RTC_DS1307 rtc;

byte paleta = 2;
byte modo   = 3;

CRGB corona[NUM_LEDS];

// Temporizadores
long tmpAntes = 0;

ClickButton boton1(BOTON1_PIN, HIGH, CLICKBTN_PULLUP);

void setup() {
    inicializacion();
}

void loop() {
  DateTime ahora = rtc.now();
  byte brillo=map(analogRead(SENSOR_LUZ_PIN),0,1023,BRILLO_MIN,BRILLO_MAX);
  
  switch(modo){
    case 0: modoNormal(ahora,paleta,brillo); break;
    case 1: modoInvertido(ahora,paleta,brillo); break;
    case 2: modoArcos(ahora,paleta,brillo); break;
    case 3: modoEstelas(ahora,paleta,brillo); break;
    default: modo = 0; break;
  }
  
  boton1.Update();
  
  if(boton1.clicks == 1) {
    if (paleta < TOTAL_PALETAS-1) {
      paleta++;
    }
    else {
      paleta = 0;
    }    
  }
  
  if(boton1.clicks == -1) {
    if (modo < TOTAL_MODOS-1) {
      modo++;
    }
    else {
      modo = 0;
    }    
  }

  FastLED.setDither(BINARY_DITHER);
  FastLED.setBrightness(brillo);
  FastLED.show();
  
   /*   unsigned long tmpAhora = millis();
   if(tmpAhora - tmpAntes > 250) {
    tmpAntes = tmpAhora;  
    //Hago cosas
  }*/
  
  if (DEBUG > 0) {
    Serial.println("====================================================");
    relojSerial(ahora);
    Serial.print("  -  ");
    Serial.print(FastLED.getFPS());
    Serial.print(" FPS  -  Paleta ");
    Serial.print(paleta);
    Serial.print("  -  Modo ");
    Serial.println(modo);
    Serial.println("====================================================");
  }
}
